<?php

/*
 * Singleton class that manages the editors to be used within the widget.
 */
require_once("configs/widget_config.php");
require_once("editorinterface.php");
require_once("utilities.php");

class EditorManager {
    
    private static $instance;
    private static $editorList = array();
    
    // Singleton instance retrieval method
    public static function instance() {
        // if not set and null (first run)
        if(!isset(self::$instance) && is_null(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }    
    
    private function _construct() {
        $this->loadEditors();
    }
    
    /*
     * Loads configured editors from the configuration file and puts them
     * in an array.
     */
    public function loadEditors() {
        $editorList = Config::getEditorList();
        foreach($editorList as $editor) {
            $path = "editors/".$editor.".php";      // Path to editor driver            
            include_once($path);
            if(!isset(self::$editorList[$editor])) {        //is driver not added
                $editorInstance = new $editor();
                self::$editorList[$editorInstance->getEditorID()] = $editorInstance; // driver added as [editorID] =>  [editor]
            }
        }
    }
    
    /*
     * Lists all loaded editors
     */
    public function getLoadedEditorIDs() {
        $arr = array();
        foreach(self::$editorList as $editor) {
            array_push($arr, $editor->getEditorID());
        }
        return $arr;
    }
    
    /*
     * Wrapper method to output loaded editors as XML
     */
    public function getLoadedEditorsIDsXML() {
        $arr = $this->getLoadedEditorIDs();
        $dom = quickDom();
        $editors = $dom->createElement("editors");
        $dom->appendChild($editors);
        foreach($arr as $ed) {
            $editors->appendChild($dom->createElement("editor", $ed));
        }
        return $dom;
    }
    
    /*
     * Get all categories across all editors, removing duplicates.
     */
    public function getGlobalCategories() {
        $categoryArray = array();
        foreach(self::$editorList as $editor) {
            $nodes = $editor->getCategories()->getElementsByTagName("category");
            foreach($nodes as $node) {
                array_push($categoryArray, $node->nodeValue);
            }
        }
        $categories_unique = array_unique($categoryArray);             // removes duplicates
        return arrayToXml("categories", "category", $categories_unique);
    }
    
    /*
     * Gets all titles across all editors, removing duplicates.
     */
    public function getGlobalTitlesByCategory($category) {
        $titleArray = array();
        foreach(self::$editorList as $editor) {
            $nodes = $editor->getTitlesByCategory($category)->getElementsByTagName("title");
            foreach($nodes as $node) {
                array_push($titleArray, $node->nodeValue);
            }
        }
        $titles_unique = array_unique($titleArray);         // removes duplicates
        return arrayToXml("titles", "title", $titles_unique);
    }
    
    /*
     * Gets the toplists from all editors
     */
    public function getGlobalTopTitles($quantity) {
        $dom = quickDom();
        $root = $dom->createElement("toptitles");
        $dom->appendChild($root);
        foreach(self::$editorList as $editor) {
            // recursivelly append to the created root elemement all nodes retrived by $editor->getTopTitles()
            $root->appendChild($dom->importNode($editor->getTopTitles($quantity)->documentElement,true));
        }
        return $dom;        
    }
    
    /*
     * Gets the top titles for a certain editor, filtered by max amount retrived
     */
    public function getTopTitles($editorID, $quantity) {
        if(isset(self::$editorList[$editorID])) {
            return self::$editorList[$editorID]->getTopTitles($quantity);
        } else {
            return $this->getErrorUnkown();
        }
    }
    
    /*
     * Gets the title details for a certain book
     */
    public function getTitleDetails($editorID, $bookTitle) {
        if(isset(self::$editorList[$editorID])) {
            return self::$editorList[$editorID]->getDetails($bookTitle);
        } else {
            return $this->getErrorUnkown();
        }
    }
    
    public function getErrorUnkown() {
        $dom = quickDom();
        $dom->appendChild($dom->createElement("widget", "unknown editor"));
        return $dom;
    }
    
}