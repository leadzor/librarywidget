<?php

/*
 * Database Request Logger configuration file
 */

define("DBHOST", "localhost");   // The Database host address
define("DBUSER", "root");   // The Database username
define("DBPASS", "a1b2c3d4");   // The Database password
define("DBNAME", "librarywidget");   // The Database name
define("DBTABL", "widgetlog");   // The Database table
