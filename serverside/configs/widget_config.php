<?php

class Config {
    public static $defaultNumItems = 4;
    public static $defaultNumToTitles = 10;
    public static $defaultNumItemsPerPage = 4;
    
    private static $editorList = array('Editora1','Editora2','EditoralCoolPen');
    private static $xmlConfig = array (
            "version" => "1.0",
            "encoding" => "ISO-8859-1");

    
    
    /*
     * DO NOT CHANGE BELOW THIS COMMENT
     * -----------------------------------------------------------
     */
    public static function getEditorList() {
        return self::$editorList;
    }
    
    public static function xmlConfig() {
        return self::$xmlConfig;
    }
}