/*
 * Script de manutencao client-side da widget de biblioteca para a disciplina de
 * Arquitetura de sistemas. Responsavel por validacoes e requests AJAX ao servidor
 * de dados, bem como alteracao de campos com dados recebidos.
 * Pedro Leal ( 1110863 )
 * Joao Ramos ( 1110364 )
 * Departamento de Engenharia Informatica - ISEP, Outubro 2013
 * 
 * [!!Nota: script nao optimizado para transferencia!!]
 */
var xmlHttpCategories;
var xmlHttpListFilter;
var xmlHttpTopLists;
var xmlHttpDetails;
var xmlHttpEditors;

var widFacade = "http://uvm044.dei.isep.ipp.pt/~webserver/serverside/widgetfacade.php";
var requestCategories       = widFacade + "?category=";
var requestListCategories   = requestCategories + "all";
var requestTopBooks         = widFacade + "?top=";
var requestDetails          = widFacade;
var requestEditors          = widFacade + "?editor=";
var requestAllEditors       = requestEditors + "all";
/*
 * Responsavel por efetuar os pedidos de AJAX ao servidor
 * @param {type} url o url para onde se devem enviar os requests
 * @param {type} handle a acao a despoletar apos o recebimento da resposta
 */
function MakeXMLHTTPCall(url,handle,xmlHttpObj) {
    if(xmlHttpObj) {
        xmlHttpObj.open("GET",url);
        xmlHttpObj.onreadystatechange = handle;
        xmlHttpObj.send(null);
    }
}

function CreateXmlHttpRequestObject() {
    xmlHttpObj = null;
    if (window.XMLHttpRequest){
        xmlHttpObj = new XMLHttpRequest();         
    }else if (window.ActiveXObject) {
        xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlHttpObj;
}
/*
 * Event Listeners
 *      Responsaveis por escutar eventos, e despuletar as acoes pretendidas
 */

// bootstrap() prepara a página para ser mostrada, escutando pelo evento _onload_
function bootstrap() {
    fillCategoriesEvent();
    fillEditorsEvent();
}

function sendPreferences() {
    var numbooks = $("#numtopbooks").spinner("value");
    var numTop = $("#numtopbooks").spinner("value");
    $.post(widFacade, {num_b:numbooks,num_tb:numTop}).done(function() {console.log("POST")});
}

// even that fires the XHR for the category filling
function fillCategoriesEvent() {
    xmlHttpCategories = CreateXmlHttpRequestObject();
    MakeXMLHTTPCall(requestListCategories, fillCategoriesAction, xmlHttpCategories);
}

// even that fires the XHR for the editor filling
function fillEditorsEvent() {
    xmlHttpEditors = CreateXmlHttpRequestObject();
    MakeXMLHTTPCall(requestAllEditors, fillEditorsAction, xmlHttpEditors);
}

// fillTitlesEvent() fica a escuta do evento de alteracao de parametros de filtragem (categoria e/ou numero)
function fillTitlesEvent() {
    var s = document.getElementById("categories");
    var selected = s.options[s.selectedIndex].value;
    xmlHttpListFilter = CreateXmlHttpRequestObject();
    sendPreferences();
    MakeXMLHTTPCall(requestCategories+selected, fillTitlesAction, xmlHttpListFilter);
    
}

function fillTopListsEvent() {
    xmlHttpTopLists = CreateXmlHttpRequestObject();
    var numbooks = $("#numtopbooks").spinner("value");//document.getElementById("numtopbooks").value;
    var s = document.getElementById("editors");
    var editor = s.options[s.selectedIndex].value;
    //VALIDAR!!!
    MakeXMLHTTPCall(requestTopBooks+numbooks+"&editor="+editor, fillTopsAction, xmlHttpTopLists);
    sendPreferences();
}

function showDetailsEvent(title, editorID) {
    xmlHttpDetails = CreateXmlHttpRequestObject();
    var encodedTitle = encodeURI(title);
    var queryString = widFacade+"?editor="+editorID+"&title="+encodedTitle;
    MakeXMLHTTPCall(queryString, showDetailsAction, xmlHttpDetails);
}
// #################################################################################################
// #################################################################################################
/*
 * Handlers
 *      Responsaveis por efetuar acoes quando requisitados pelos even listeners
 */
function fillCategoriesAction() {
    if(xmlHttpCategories.readyState === 4 && xmlHttpCategories.status === 200) {
        var doc = xmlHttpCategories.responseXML;
        fillCategories(doc);
    }
}

function fillEditorsAction() {
    if(xmlHttpEditors.readyState === 4 && xmlHttpEditors.status === 200) {
        var doc = xmlHttpEditors.responseXML;
        fillEditors(doc);
    }
}

/*
 * Fills the title list based on the selected category
 */
function fillTitlesAction() {
    if(xmlHttpListFilter.readyState === 4 && xmlHttpListFilter.status === 200) {
        var doc = xmlHttpListFilter.responseXML;
        fillTitles(doc);
    }
}

// fills the top titles
function fillTopsAction() {
    if(xmlHttpTopLists.readyState === 4 && xmlHttpTopLists.status === 200) {
        var doc = xmlHttpTopLists.responseXML;
        fillTops(doc);
    }
}

function showDetailsAction() {
    if(xmlHttpDetails.readyState === 4 && xmlHttpDetails.status === 200) {
        var doc = xmlHttpDetails.responseXML;
        showDetails(doc);
    }
}