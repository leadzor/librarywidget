
var itemsPerPage = 4;//"<?php echo $itemsPerPage; ?>";
var pages = new Array();
var pageCounter = 0;
var currentPage = 0;
/*
 * Fills the categories dropdown box
 */
function fillCategories(xmldoc) {
    var categories_xml = xmldoc.getElementsByTagName("category");
    var select_html = document.getElementById("categories");

    select_html.innerHTML = '';      // cleanup
    select_html.appendChild(document.createElement("option"));   // Set default selected to empty

    for (var i = 0; i < categories_xml.length; i++) {
        var text = categories_xml[i].childNodes[0].nodeValue;
        var node = document.createElement("option");
        node.setAttribute("value", text);
        node.appendChild(document.createTextNode(text));
        select_html.appendChild(node);
    }
}

function fillEditors(xmldoc) {
    var editors_xml = xmldoc.getElementsByTagName("editor");
    var select_html = document.getElementById("editors");

    select_html.innerHTML = '';      // cleanup
    select_html.appendChild(document.createElement("option"));   // Set default selected to empty

    for (var i = 0; i < editors_xml.length; i++) {
        var text = editors_xml[i].childNodes[0].nodeValue;
        var node = document.createElement("option");
        node.setAttribute("value", text);
        node.appendChild(document.createTextNode(text));
        select_html.appendChild(node);
    }
}

/*
 * Fills the title list based on the selected category
 */
function fillTitles(xmldoc) {
    var titles_xml = xmldoc.getElementsByTagName("title");
    var titleList_html = document.getElementById("titleList");
    var maxbooks = document.getElementById("numitems").value;
    var ul_html = document.createElement("ul");
    titleList_html.innerHTML = '';
    titleList_html.appendChild(ul_html);
    //FIXME validacaode input!!!

    for (var i = 0; i < titles_xml.length && i < maxbooks; i++) {
        var text = titles_xml[i].childNodes[0].nodeValue;
        var item_html = document.createElement("li");
        item_html.innerHTML = text;
        ul_html.appendChild(item_html);
    }
}

/*
 * Paging and top listing system
 */
// displays next item page
function prevPage() {
    var pageFrame = document.getElementById("pageframe");
    if (currentPage > 0) {
        var ul = pages[--currentPage];
        pageFrame.innerHTML = '';
        pageFrame.appendChild(ul);
    }
}

// displays previous item page
function nextPage() {
    var pageFrame = document.getElementById("pageframe");
    if (currentPage < pages.length - 1) {
        var ul = pages[++currentPage];
        pageFrame.innerHTML = '';
        pageFrame.appendChild(ul);
    }
}

/*
 * Sets viewing page to a page #n,if in range.
 */
function setPage(pagen) {
    var pageFrame = document.getElementById("pageframe");
    if (pagen > -1 && pagen < pages.length) {
        currentPage = pagen;
        var ul = pages[currentPage];
        pageFrame.innerHTML = '';
        pageFrame.appendChild(ul);
    }
}

// creates an anchor with the onclick attribute set to execute showDetailsEvent
function makeLinkToDetails(title, editorID) {
    var anchor = document.createElement("a");
    anchor.href = "#";
    anchor.innerHTML = title;
    anchor.setAttribute("onclick", "showDetailsEvent(\"" + title + "\", \"" + editorID + "\")");
    return anchor;
}

// fills the top titles
function fillTops(xmldoc) {
    pages.length = 0;           // clears the page array
    pageCounter = 0;            // resets the page counter
    currentPage = 0;            // sets current page to 0
    var elements_xml = xmldoc.getElementsByTagName("title");
    var pageFrame_html = document.getElementById("pageframe");
    pageFrame_html.innerHTML = '';

    for (var i = 0; i < elements_xml.length; ) {
        var pageList = document.createElement("ul");
        // fills the pages until max item per page or number is available elements is reached.
        for (var j = 0; j < itemsPerPage && i < elements_xml.length; j++, i++) {
            var title = elements_xml[i].childNodes[0];
            var pageItem = document.createElement("li");
            var titleText = title.nodeValue;
            var editor = elements_xml[i].parentNode.getAttribute("id");
            pageItem.appendChild(makeLinkToDetails(titleText, editor));
            pageList.appendChild(pageItem);
        }
        pages[pageCounter++] = pageList;
    }
    pageFrame_html.appendChild(pages[0]);
}

/*
 * Shows details of a certain title.
 */
function showDetails(xmldoc) {
    var toplistdiv = document.getElementById("pageframe");

    var ptitle = document.createElement("p");
    var pauthor = document.createElement("p");
    var pcategory = document.createElement("p");
    var pisbn = document.createElement("p");
    var pyear = document.createElement("p");

    var prev = document.createElement("input");
    var syn = document.createElement("input");
    prev.type = "button";
    prev.value = "back to titles";
    prev.setAttribute("onclick", "setPage(currentPage)");
    syn.type = "button";
    syn.value = "Sinopse";
    syn.setAttribute("onclick", "launchSynPopup()");

    toplistdiv.innerHTML = '';
    toplistdiv.appendChild(document.createElement("hr"));

    toplistdiv.appendChild(ptitle);
    toplistdiv.appendChild(pauthor);
    toplistdiv.appendChild(pcategory);
    toplistdiv.appendChild(pisbn);
    toplistdiv.appendChild(pyear);

    toplistdiv.appendChild(syn);
    toplistdiv.appendChild(prev);

    var xmdoc = xmldoc.getElementsByTagName("book")[0];

    var bookTitle = xmdoc.getElementsByTagName("title")[0].childNodes[0].nodeValue;
    var bookAuthor = xmdoc.getElementsByTagName("author")[0].childNodes[0].nodeValue;
    var bookCategory = xmdoc.getElementsByTagName("category")[0].childNodes[0].nodeValue;
    var bookIsbn = xmdoc.getElementsByTagName("isbn")[0].childNodes[0].nodeValue;
    var bookYear = xmdoc.getElementsByTagName("publicacao")[0].childNodes[0].nodeValue;

    ptitle.innerHTML = "Titulo: ";// + bookTitle;
    pauthor.innerHTML = "Autor: " + bookAuthor;
    pcategory.innerHTML = "Categoria: " + bookCategory;
    pisbn.innerHTML = "ISBN: ";// + bookIsbn;
    pyear.innerHTML = "Ano: " + bookYear;
    
    var spantitle = document.createElement("span");
    spantitle.innerHTML = bookTitle;
    
    var spanisbn = document.createElement("span");
    spanisbn.innerHTML = bookIsbn;
    
    spanisbn.setAttribute("id", "isbn");
    spantitle.setAttribute("id", "title");
    
    ptitle.appendChild(spantitle);
    pisbn.appendChild(spanisbn);

}

function launchSynPopup() {
    var windowObjectReference;
    var strWindowFeatures = "menubar=no,location=yes,resizable=yes,scrollbars=yes,status=no,width=800,height=600";
    windowObjectReference = window.open("popup_jsonquery.html", "Sinopse", strWindowFeatures);
}
