<?php

interface IEditor {
    
    public function getCategories();
    
    public function getTitlesByCategory($category);
    
    public function getDetails($title);
    
    public function getTopTitles($number);
    
    public function getEditorID();
    
    
}