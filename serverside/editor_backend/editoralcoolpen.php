<?php

$storedBOOKS = array( 
    "Aventura" => array(
        "Harry Potter and the Deadly Hallows" => array(
            "title" => "Harry Potter and the Deadly Hallows",
            "category" => "Aventura",
            "isbn" => "408810298",
            "author" => "J.K. Rowling",
            "publicacao" => "2007"
        )
    ),
    "Policial" => array(
        "Digital Fortress" => array(
            "title" => "Digital Fortess",
            "category" => "Policial",
            "isbn" => "0552169986",
            "author" => "Dan Brown",
            "publicacao" => "1998"
        ),
        "The Da Vinci Code" => array (
            "title" => "The Da Vinci Code",
            "category" => "Policial",
            "isbn" => "0307474275",
            "author" => "Dan Brown",
            "publicacao" => "2009"
        )
    ) );


if(isset($_GET['categoria'])) {
    if(isset($storedBOOKS[$_GET['categoria']])) {
        echo getFromCategory($_GET['categoria']);
    } else if ($_GET['categoria'] == "todas") {
        echo getCategories();
    } else {
        echo jsonEmpty();
    }
}else if(isset($_GET['titulo'])) {
    echo getDetails($_GET['titulo']);
} else {
    echo "{\"comando\":\"invalido\"}";
}

function getDetails($title) {
    global $storedBOOKS;
    foreach($storedBOOKS as $category => $bookarr) {
        if(isset($bookarr[$title])) {
            return json_encode($bookarr[$title]);
        }
    }
    return jsonEmpty();
}

function getCategories() {
    global $storedBOOKS;
    $arr = array();
    foreach($storedBOOKS as $cat => $bootArr) {
        array_push($arr, $cat);
    }
    return json_encode($arr);
}

function getFromCategory($category) {
    global $storedBOOKS;
    return json_encode($storedBOOKS[$category]);
}

function jsonEmpty() {
    return "{\"pesquisa\":\"vazio\"}";
}