<?php
require_once("editorinterface.php");
require_once("utilities.php");
require_once("configs/widget_config.php");

class Editora1 implements IEditor {
    
    private static $id          = "editora1";
    private static $dataUrl     = "http://phpdev2.dei.isep.ipp.pt/~arqsi/trabalho1/editora1.php";
    
    private static $cmd_categories  = "?categoria=";
    private static $cmd_title       = "?titulo=";
    private static $cmd_number      = "?numero=";
    
    public function __construct() {
    }
    
    /*
     * Get all categories from the editor server
     */
    public function getCategories() {
        // begin remote data retrieval
        $remoteXml      = quickDom();
        $remoteCommand  = self::$dataUrl.self::$cmd_categories."todas";
        $remoteString   = quickRemoteXml($remoteCommand, self::$id);
        $remoteXml->loadXML($remoteString);
        
        // local xml initialization
        $localXml   = quickDom();
        $localRoot  = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        
        // remote to local element translation
        $remoteNodes = $remoteXml->getElementsByTagName("categoria");
        foreach($remoteNodes as $node) {
            $localRoot->appendChild($localXml->createElement("category", $node->nodeValue));
        }
        return $localXml;//->saveXML();
    }
    
    /*
     * Get details from a certain title from the editor server
     */
    public function getDetails($title) {
        $title_enc      = rawurlencode($title);
        $remoteXml      = quickDom();
        $remoteCommand  = self::$dataUrl.self::$cmd_title.$title_enc;
        $remoteString   = quickRemoteXml($remoteCommand, "editor");
        $remoteXml->loadXML($remoteString);
        $remoteXml->documentElement->setAttribute("id", self::$id);
        return $remoteXml;//->saveXML();
    }

    /*
     * returns the editor ID
     */
    public function getEditorID() {
        return self::$id;
    }

    /*
     * Gets all titles from a certain category from the editor server
     */
    public function getTitlesByCategory($category) {
        $cat_enc      = rawurlencode($category);
       // begin remote data retrieval
        $remoteXml      = quickDom();
        $remoteCommand  = self::$dataUrl.self::$cmd_categories.$cat_enc;
        $remoteString   = quickRemoteXml($remoteCommand, self::$id);
        $remoteXml->loadXML($remoteString);
        
        // local xml initialization
        $localXml   = quickDom();
        $localRoot  = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        
        // remote to local element translation
        $remoteNodes = $remoteXml->getElementsByTagName("title");
        foreach($remoteNodes as $node) {
            $localRoot->appendChild($localXml->createElement("title", $node->nodeValue));
        }
        return $localXml;//->saveXML();
    }

    /*
     * gets all n top titles from the editor server
     */
    public function getTopTitles($number) {
        // begin remote data retrieval
        $remoteXml      = quickDom();
        $remoteCommand  = self::$dataUrl.self::$cmd_number.$number;
        $remoteString   = quickRemoteXml($remoteCommand, self::$id);
        $remoteXml->loadXML($remoteString);
        
        // local xml initialization
        $localXml   = quickDom();
        $localRoot  = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        
        // remote to local element translation
        $remoteNodes = $remoteXml->getElementsByTagName("title");
        foreach($remoteNodes as $node) {
            $localRoot->appendChild($localXml->createElement("title", $node->nodeValue));
        }
        return $localXml;//->saveXML();
    }

}

