<?php
require_once("editorinterface.php");
require_once("utilities.php");
require_once("configs/widget_config.php");

class EditoralCoolPen implements IEditor {
    
    private static $id          = "editoralcoolpen";
    private static $dataUrl     = "http://phpdev2.dei.isep.ipp.pt/~i110863/editoralcoolpen.php";
    
    private static $cmd_categories  = "?categoria=";
    private static $cmd_title       = "?titulo=";
    
    public function __construct() {
    }
    
    /*
     * Get all categories from the editor server
     */
    public function getCategories() {
        // begin remote data retrieval
        $remoteCommand  = self::$dataUrl.self::$cmd_categories."todas";
        $remoteJson   = file_get_contents($remoteCommand);
        $remoteData     = json_decode($remoteJson);
        // local xml initialization
        $localXml   = quickDom();
        $localRoot  = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        
        if(isset($remoteData['pesquisa']) || isset($remoteData['comando'])) {
            return $localXml;
        }
        
        // remote to local element translation
        foreach($remoteData as $data) {
            $localRoot->appendChild($localXml->createElement("category", $data));
        }
        return $localXml;//->saveXML();
    }
    
    /*
     * Get details from a certain title from the editor server
     */
    public function getDetails($title) {
        $title_enc      = rawurlencode($title);
        $remoteCommand  = self::$dataUrl.self::$cmd_title.$title_enc;
        $remoteJson   = file_get_contents($remoteCommand);
        $remoteData = json_decode($remoteJson, true);    
        
        $localXml = quickDom();
        $localRoot = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        
        if(isset($remoteData['pesquisa']) || isset($remoteData['comando'])) {
            return $localXml;
        }
        
        foreach($remoteData as $field => $value) {
            $element = $localXml->createElement($field, $value);
            $localRoot->appendChild($element);
        }
        return $localXml;//->saveXML();
    }

    /*
     * returns the editor ID
     */
    public function getEditorID() {
        return self::$id;
    }

    /*
     * Gets all titles from a certain category from the editor server
     */
    public function getTitlesByCategory($category) {
        $cat_enc = urlencode($category);
        $remoteCommand  = self::$dataUrl.self::$cmd_categories.$cat_enc;
        $remoteJson   = file_get_contents($remoteCommand);
        $remoteData = json_decode($remoteJson,true);
        
        // local xml initialization
        $localXml   = quickDom();
        $localRoot  = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        
        if(isset($remoteData['pesquisa']) || isset($remoteData['comando'])) {
            return $localXml;
        }
        foreach($remoteData as $book) {
            $localRoot->appendChild($localXml->createElement("title", $book['title']));
        }
        
        return $localXml;
    }

    /*
     * gets all n top titles from the editor server
     */
    public function getTopTitles($number) {
        // local xml initialization
        $localXml   = quickDom();
        $localRoot  = $localXml->createElement("editor");
        $localRoot->setAttribute("id", self::$id);
        $localXml->appendChild($localRoot);
        return $localXml;
    }

}

