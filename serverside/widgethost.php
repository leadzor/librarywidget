<?php
require_once("configs/widget_config.php");
session_start();
if(isset($_SESSION['numTitles'])) {
    $numTitles = $_SESSION['numTitles'];
} else {
    $numTitles = Config::$defaultNumItems;
}
if(isset($_SESSION['numTopTitles'])) {
    $numTopTitles = $_SESSION['numTopTitles'];
} else {
    $numTopTitles = Config::$defaultNumToTitles;
}
if(isset($_SESSION['itemsPerPage'])) {
    $itemsPerPage = $_SESSION['itemsPerPage'];
} else {
    $itemsPerPage = Config::$defaultNumItemsPerPage;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/dot-luv/jquery-ui-1.10.3.custom.css">
        <script type="text/javascript" src="js/ajaxcalls.js"></script>
        <script type="text/javascript" src="js/docHandler.js"></script>

        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script>
            (function($) {
                $.widget("custom.combobox", {
                    _create: function() {
                        this.wrapper = $("<span>")
                                .addClass("custom-combobox")
                                .insertAfter(this.element);

                        this.element.hide();
                        this._createAutocomplete();
                        this._createShowAllButton();
                    },
                    _createAutocomplete: function() {
                        var selected = this.element.children(":selected"),
                                value = selected.val() ? selected.text() : "";

                        this.input = $("<input>")
                                .appendTo(this.wrapper)
                                .val(value)
                                .attr("title", "")
                                .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                                .autocomplete({
                                    delay: 0,
                                    minLength: 0,
                                    source: $.proxy(this, "_source")
                                })
                                .tooltip({
                                    tooltipClass: "ui-state-highlight"
                                });

                        this._on(this.input, {
                            autocompleteselect: function(event, ui) {
                                ui.item.option.selected = true;
                                this._trigger("select", event, {
                                    item: ui.item.option
                                });
                            },
                            autocompletechange: "_removeIfInvalid"
                        });
                    },
                    _createShowAllButton: function() {
                        var input = this.input,
                                wasOpen = false;

                        $("<a>")
                                .attr("tabIndex", -1)
                                .attr("title", "Show All Items")
                                .tooltip()
                                .appendTo(this.wrapper)
                                .button({
                                    icons: {
                                        primary: "ui-icon-triangle-1-s"
                                    },
                                    text: false
                                })
                                .removeClass("ui-corner-all")
                                .addClass("custom-combobox-toggle ui-corner-right")
                                .mousedown(function() {
                                    wasOpen = input.autocomplete("widget").is(":visible");
                                })
                                .click(function() {
                                    input.focus();

                                    // Close if already visible
                                    if (wasOpen) {
                                        return;
                                    }

                                    // Pass empty string as value to search for, displaying all results
                                    input.autocomplete("search", "");
                                });
                    },
                    _source: function(request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response(this.element.children("option").map(function() {
                            var text = $(this).text();
                            if (this.value && (!request.term || matcher.test(text)))
                                return {
                                    label: text,
                                    value: text,
                                    option: this
                                };
                        }));
                    },
                    _removeIfInvalid: function(event, ui) {

                        // Selected an item, nothing to do
                        if (ui.item) {
                            return;
                        }

                        // Search for a match (case-insensitive)
                        var value = this.input.val(),
                                valueLowerCase = value.toLowerCase(),
                                valid = false;
                        this.element.children("option").each(function() {
                            if ($(this).text().toLowerCase() === valueLowerCase) {
                                this.selected = valid = true;
                                return false;
                            }
                        });

                        // Found a match, nothing to do
                        if (valid) {
                            return;
                        }

                        // Remove invalid value
                        this.input
                                .val("")
                                .attr("title", value + " didn't match any item")
                                .tooltip("open");
                        this.element.val("");
                        this._delay(function() {
                            this.input.tooltip("close").attr("title", "");
                        }, 2500);
                        this.input.data("ui-autocomplete").term = "";
                    },
                    _destroy: function() {
                        this.wrapper.remove();
                        this.element.show();
                    }
                });
            })(jQuery);

            $(function() {
                $("#combobox").combobox();
                $("#toggle").click(function() {
                    $("#combobox").toggle();
                });
            });
        </script>
        <style>
            .custom-combobox {
                position: relative;
                display: inline-block;
            }
            .custom-combobox-toggle {
                position: absolute;
                top: 0;
                bottom: 0;
                margin-left: -1px;
                padding: 0;
                /* support: IE7 */
                *height: 1.7em;
                *top: 0.1em;
            }
            .custom-combobox-input {
                margin: 0;
                padding: 0.3em;
            }
        </style>

    </head>
    <body onload="bootstrap()">
        <div id="booktitlelist">
            <div id="tabs">
                <ul>
                    <li><a href="#fragment-1"><span>Categorias</span></a></li>
                    <li><a href="#fragment-2"><span>Tops Editora</span></a></li>
                </ul>
                <div id="fragment-1">
                    <form>
                        <p>Categorias: <select id="categories"></select></p>   
                        <p>Nr items: <input id="numitems" class="spinners" readonly="readobly"/></p>
                        <p><input type="button" onclick="fillTitlesEvent()" value="Carregar" /></p>
                    </form>
                    <div id="titleList"></div>
                </div>
                <div id="fragment-2">
                    <form>
                        <p>Quantidade: 
                            <input id="numtopbooks" class="spinners" readonly="readobly"/>
                            <!--<input type="text" id="numtopbooks" />-->
                        </p>
                        <p>
                            Editora: <select id="editors"></select>
                        </p>
                        <p><input type="button" onclick="fillTopListsEvent()" value="Carregar" /></p>
                    </form>
                    <div id="toplists">
                        <div id="pageframe"></div>
                        <p>
                            <input type="button" id="prev" onclick="prevPage()" value="<"/>
                            <input type="button" id="next" onclick="nextPage()" value=">"/>
                        </p>
                    </div>
                </div>
            </div>
            <script>
                $("select").combobox();
                $("#tabs").tabs();
                $(".spinners").spinner({min: 0});
                $(".spinners").spinner("option", "max", 50);
                $("#numitems").spinner("value", <?php echo $numTitles; ?>);
                $("#numtopbooks").spinner("value", <?php echo $numTopTitles; ?>);
            </script>
        </div>
    </body>
</html>