<?php

include("editorinterface.php");
include("editors/Editora1.php");
include("editors/Editora2.php");
include("editors/EditoralCoolPen.php");
include("editormanager.php");
$e = new Editora1();

print ("<h1>Editora1 Test</h1>");
print("getCategories()");
echo htmlspecialchars($e->getCategories()->saveXML());
print("<hr />");
print("getDetails(\"".rawurlencode("Os Maias")."\")");
echo htmlspecialchars($e->getDetails("Os Maias")->saveXML());
print("<hr />");
print("getID()");
echo htmlspecialchars($e->getEditorID());
print("<hr />");
print("getCategory(\"Software\")");
echo htmlspecialchars($e->getTitlesByCategory("Software")->saveXML());
print("<hr />");
print("getTopTitles(3)");
echo htmlspecialchars($e->getTopTitles(3)->saveXML());
print("<hr />");
print("<br />");

$a = new Editora2();
print ("<h1>Editora2 Test</h1>");

print("getCategories()");
echo htmlspecialchars($a->getCategories()->saveXML());
print("<hr />");
print("getDetails(\"Tabacaria\")");
echo htmlspecialchars($a->getDetails("Os Maias")->saveXML());
print("<hr />");
print("getID()");
echo htmlspecialchars($a->getEditorID());
print("<hr />");
print("getCategory(\"Software\")");
echo htmlspecialchars($a->getTitlesByCategory("Software")->saveXML());
print("<hr />");
print("getTopTitles(3)");
echo htmlspecialchars($a->getTopTitles(3)->saveXML());
print("<hr />");
print("<br />");

$b = new EditoralCoolPen();
print ("<h1>EditoralCoolPen Test</h1>");

print("getCategories()");
echo htmlspecialchars($b->getCategories()->saveXML());
print("<hr />");
print("getDetails(\"Digital Fortress\")");
echo htmlspecialchars($b->getDetails("Digital Fortress")->saveXML());
print("<hr />");
print("getID()");
echo htmlspecialchars($b->getEditorID());
print("<hr />");
print("getCategory(\"Policial\")");
echo htmlspecialchars($b->getTitlesByCategory("Policial")->saveXML());
print("<hr />");
print("getTopTitles(3)");
echo htmlspecialchars($b->getTopTitles(3)->saveXML());
print("<hr />");
print("<br />");

print ("<h1>EditorManager Test</h1>");
$eman = EditorManager::instance();
$eman->loadEditors();
print("<hr />getLoadedEditorIDs()");
print_r($eman->getLoadedEditorIDs());
print("<hr />getGlobalCategories()");
print( htmlspecialchars($eman->getGlobalCategories()->saveXml()));
print("<hr />getGlobalTitlesByCategory(Software)");
print( htmlspecialchars($eman->getGlobalTitlesByCategory("Software")->saveXml()));
print("<hr />getGlobalTopTitles(2)");
print( htmlspecialchars($eman->getGlobalTopTitles(2)->saveXml()));
print("<hr />getTopTitles(editora1, 3)");
print( htmlspecialchars($eman->getTopTitles("editora1", 3)->saveXml()));
print("<hr />getTopTitles(editora0, 3)");
print( htmlspecialchars($eman->getTopTitles("editora0", 3)->saveXml()));
print("<hr />getDetails(Editor1, Tabacaria)");
print( htmlspecialchars($eman->getTitleDetails("editora1", "Tabacaria")->saveXml()));
print("<hr />getDetails(Editor0, Tabacaria)");
print( htmlspecialchars($eman->getTitleDetails("editor0", "Tabacaria")->saveXml()));