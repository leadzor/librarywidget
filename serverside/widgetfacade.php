<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
include_once ("dbrequestlogger.php");
include_once ("utilities.php");
include_once ("editormanager.php");
include_once ("configs/widget_config.php");

$EDI_MANAGER = EditorManager::instance();
$LOG_MANAGER = DBRequestLogger::instance();

$query = $_SERVER['QUERY_STRING'];
$source = $_SERVER['REMOTE_ADDR'];
$reqlevel;

// PREFERENCE SETTING

if(isset($_POST['num_b']) && isset($_POST['num_tb'])) {
    $nb = $_POST['num_b'];
    $ntb = $_POST['num_tb'];
    $_SESSION['numTitles'] = $nb;
    $_SESSION['numTopTitles'] = $ntb;
}

// If is a request for categories or titles organized by categories
if(isset($_GET['category'])) {
    if($_GET['category'] == "all") {
        getAllCategories();
    } else {
        getTitlesByCategory($_GET['category']);
    }
} else if(isset($_GET['editor'])) {
        $editor = $_GET['editor'];
        if($editor == "all") {
            getAllEditors();
        } else if(isset($_GET['top']) && $editor != "all") {
            getTopTitles($editor, $_GET['top']);
        } else if(isset($_GET['title']) && $editor != "all") {
            getDetails($editor, $_GET['title']);
        }
} else if(isset($_GET['top'])) {
    getGlobalTopTitles($_GET['top']);
}
$reqlevel = DBRL_REQUEST;
// for debug and testing purposes
if(isset($_GET['debug'])) {
    $reqlevel = DBRL_TEDEBUG;
}

// Database insertion goes here.
$LOG_MANAGER->log($reqlevel, $query, $source);

function getAllCategories() {
    global $EDI_MANAGER;
    $EDI_MANAGER->loadEditors();
    
    header("Content-type: text/xml; charset=ISO-8859-1");
    echo $EDI_MANAGER->getGlobalCategories()->saveXML();
}

function getAllEditors() {
    global $EDI_MANAGER;
    $EDI_MANAGER->loadEditors();
    
    header("Content-type: text/xml; charset=ISO-8859-1");
    echo $EDI_MANAGER->getLoadedEditorsIDsXML()->saveXML();
}

function getTitlesByCategory($category) {
    global $EDI_MANAGER;
    $EDI_MANAGER->loadEditors();
    
    header("Content-type: text/xml; charset=ISO-8859-1");
    echo $EDI_MANAGER->getGlobalTitlesByCategory($category)->saveXML();   
}

function getDetails($editor, $title) {
    global $EDI_MANAGER;
    $EDI_MANAGER->loadEditors();
    
    header("Content-type: text/xml; charset=ISO-8859-1");
    echo $EDI_MANAGER->getTitleDetails($editor, $title)->saveXML();  
}

function getGlobalTopTitles($quantity) {
    global $EDI_MANAGER;
    $EDI_MANAGER->loadEditors();
    
    header("Content-type: text/xml; charset=ISO-8859-1");
    echo $EDI_MANAGER->getGlobalTopTitles($quantity)->saveXML();   
}

function getTopTitles($editorID, $quantity) {
    global $EDI_MANAGER;
    $EDI_MANAGER->loadEditors();
    
    header("Content-type: text/xml; charset=ISO-8859-1");
    echo $EDI_MANAGER->getTopTitles($editorID, $quantity)->saveXML();
}