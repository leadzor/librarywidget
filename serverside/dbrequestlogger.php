<?php

require("configs/dbrequestlogger_config.php");

define("DBRL_REQUEST", 0);      // accepted request
define("DBRL_INVALID", 1);      // invalid request
define("DBRL_WARNING", 2);      // dangerous request (possible SQL injection)
define("DBRL_TEDEBUG", 3);        // debug or test information
define("DBRL_GENINFO", 4);         // general information

class DBRequestLogger {
    
    private static $instance;
    
    private static $host;
    private static $username;
    private static $passwd;
    private static $dbname;
    private static $dbtable;
    
    // Singleton instance retrieval method
    public static function instance() {
        // if not set and null (first run)
        if(!isset(self::$instance) && is_null(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    
    private function __construct() {
        $this->host         = DBHOST;
        $this->username     = DBUSER;
        $this->passwd       = DBPASS;
        $this->dbname       = DBNAME;
        $this->dbtable      = DBTABL;
    }
    
    public function log($level, $info, $source) {        
        $con = mysql_connect($this->host, $this->username, $this->passwd);        
        if (!$con) {
            echo "Erro a ligar à base de dados";
        }
        
        $sdb = mysql_select_db($this->dbname, $con);        
        if(!$sdb) {
            echo "Erro a selecionar base dados";
        }
        
        // Escaping the strings to prevent SQL Injection.
        $level_e    = mysql_real_escape_string($level);
        $info_e     = mysql_real_escape_string($info);
        $source_e   = mysql_real_escape_string($source);
        
        // Timestamp inserted automatically via SQL
        $query = "INSERT INTO $this->dbtable (LOGLEVEL,LOGDATA,LOGSOURCE) "
                ."VALUES($level_e,'$info_e','$source_e')";
        $queryReturn = mysql_query($query, $con);
        if(!$queryReturn) {
            echo mysql_error($con);
        }
        
        if(!mysql_close($con)) 
            echo mysql_error();
    }
}