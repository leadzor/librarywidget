<?php
include_once("configs/widget_config.php");

$xmlconf = Config::xmlConfig();
$version = $xmlconf["version"];
$encoding = $xmlconf["encoding"];

function get_valid_xml($source, $root, $version, $encoding) {
    $remote = file_get_contents($source);
    $header = "<?xml version=\"$version\" encoding=\"$encoding\"?>\n";
    $data = $header."<$root>".$remote."</$root>";
    return $data;
}

// Retrives headless, rootless XML from a determined source, and creates a valid XML
function quickRemoteXml($source, $root) {
    global $version;
    global $encoding;
    $remote = file_get_contents($source);
    $header = "<?xml version=\"$version\" encoding=\"$encoding\"?>\n";
    $data = $header."<$root>".$remote."</$root>";
    return $data;
}

// short for Quick DOM Document. Creates a new DOMDocument with the setting specified in the global config file.
function quickDom() {
    $xmlconf = Config::xmlConfig();
    $version = $xmlconf["version"];
    $encoding = $xmlconf["encoding"];
    $dom = new DOMDOcument($version, $encoding);
    return $dom;
}
    
function arrayToXml($root, $tagname, $array) {
    $xmlconf = Config::xmlConfig();
    $version = $xmlconf["version"];
    $encoding = $xmlconf["encoding"];
    $dom = new DOMDOcument($version, $encoding);
    $root = $dom->createElement($root);
    foreach($array as $element) {
        $root->appendChild($dom->createElement($tagname, $element));
    }
    $dom->appendChild($root);
    return $dom;
}