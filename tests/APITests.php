<?php

/* 
 * API Tests. Checks connectivity and data retriavel from the API interface
 */
$apiURL = "http://uvm044.dei.isep.ipp.pt/~webserver/serverside/widgetfacade.php?";

?>

<title>API TESTS</title>

<h5>getAllCategories()</h5>
<p><?php echo $link = $apiURL."category=all&debug";?></p>
<?php echo htmlspecialchars(file_get_contents($link)); ?>
<hr />
<h5>getTitlesByCategory("Software")</h5>
<p><?php echo $link = $apiURL."category=Software&debug";?></p>
<?php echo htmlspecialchars(file_get_contents($link)); ?>
<hr />
<h5>getGlobalTopTitles(3)</h5>
<p><?php echo $link = $apiURL."top=3&debug";?></p>
<?php echo htmlspecialchars(file_get_contents($link)); ?>
<hr />
<h5>getTopTitles("editora1",3)</h5>
<p><?php echo $link = $apiURL."top=3&editor=editora1&debug";?></p>
<?php echo htmlspecialchars(file_get_contents($link)); ?>
<hr />
<h5>getDetails("editora1","Tabacaria")</h5>
<p><?php echo $link = $apiURL."editor=editora1&title=Tabacaria&debug";?></p>
<?php echo htmlspecialchars(file_get_contents($link)); ?>
<hr />
<h5>getAllEditors()</h5>
<p><?php echo $link = $apiURL."editor=all&debug";?></p>
<?php echo htmlspecialchars(file_get_contents($link)); ?>
<hr />